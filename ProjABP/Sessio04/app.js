

var app = new Vue({
  el: '#app',
  data: {
    goals: [],
    input: ""
  },
  methods: {
    addGoal: function () {
      this.goals.push({ text: this.input });
      this.input="";
    }
  }
})

