
var formulario = new Vue({
    el: '#formulario',
    data: {
        nom: "",
        cognom: "",
        mail: "",
        tipo: "",
        alerta: ""
    },
    methods: {
        mensaje: function () {
            event.preventDefault();
            if (this.nom == "" || this.cognom == "" || this.mail == "" || this.tipo == "") {
                this.alerta = "Faltan campos por rellenar";
            } else {
                window.alert("Mensaje Enviado");
            }
        },

        img: function () {
            switch (this.tipo) {
                case "client":
                    return "https://c0.klipartz.com/pngpicture/59/673/gratis-png-iconos-de-computadora-gundam-festivales-religiosos.png";
                    break;
                case "venedor":
                    return "https://image.flaticon.com/icons/png/512/1499/1499993.png";
                    break;
                default:
                    return "";
            }
        }
    },
})