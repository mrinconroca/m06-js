const sudoku = [
    [, , , , 4, 9, , ,],
    [7, , 5, , 2, , 3, ,],
    [1, 2, 9, 3, , , , , 5],
    [, 3, , 8, , , 5, ,],
    [2, 5, , 4, , 6, , 9, 8],
    [, , 1, , , 5, , 3,],
    [8, , , , , 3, 9, 2, 7],
    [, , 2, , 8, , 6, , 3],
    [, , , 2, 9, , , ,]
]

const nums = [1, 2, 3, 4, 5, 6, 7, 8, 9];

function getNotPosibleNums(sudoku, fil, col) {
    var nums = [];

    for (var i = 0; i < 9; i++) {
        if (sudoku[fil][i] != null) {
            nums.push(sudoku[fil][i]);
        }
    }

    for (var i = 0; i < 9; i++) {
        if (sudoku[i][col] != null) {
            nums.push(sudoku[i][col]);
        }
    }

    fil = parseInt(fil / 3) * 3;
    col = parseInt(col / 3) * 3;

    for (var i = fil; i < fil + 3; i++) {
        for (var j = col; j < col + 3; j++) {
            if (sudoku[i][j] != null) {
                nums.push(sudoku[i][j]);
            }
        }
    }

    return nums;
}

function resolverSudoku(sudoku) {
    solved = false;
    while (!solved) {
        solved = true;
        for (var fil = 0; fil < 9; fil++) {
            for (var col = 0; col < 9; col++) {
                if (sudoku[fil][col] == null) {
                    solved = false;
                    var notPosible = getNotPosibleNums(sudoku, fil, col);
                    var posible = nums.filter(function (num, i) {
                        return !notPosible.includes(num);
                    })
                    if (posible.length == 1) {
                        sudoku[fil][col] = posible[0];
                    }
                }
            }
        }
    }
    return sudoku;
}
console.log("Sudoku Sin Resolver")
console.table(sudoku);
console.log("Sudoku Resuelto?")
console.table(resolverSudoku(sudoku));


