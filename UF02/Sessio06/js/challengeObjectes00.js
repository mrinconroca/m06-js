class Persona {

    constructor(nombre, peso, altura) {
        this.nombre = nombre;
        this.altura = altura;
        this.peso = peso;
    }

    calcIMC() {
        this.imc = this.peso / this.altura ** 2;
        return this.imc;
    }

}

var per1 = new Persona("Mark", 78, 1.69);
console.log("El IMC de " + per1.nombre + " es " + per1.calcIMC());
var per2 = new Persona("John", 92, 1.95);
console.log("El IMC de " + per2.nombre + " es " + per2.calcIMC());

if (per1.imc > per2.imc) {
    console.log("L'IMC de " + per1.nombre + " (" + per1.imc + ") es superior al de " + per2.nombre + " (" + per2.imc + ")!");
} else {
    console.log("L'IMC de " + per2.nombre + " (" + per2.imc + ") es superior al de " + per1.nombre + " (" + per1.imc + ")!");
}

