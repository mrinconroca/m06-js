
class Car{

    constructor(marca, velocidadInicial){
        this.marca=marca;
        this.velocidad=velocidadInicial
    }

    acelerar(){
        this.velocidad+=10;
        console.log("El coche "+this.marca+" ha acelerado, ahora va a "+this.velocidad+" Km/h");
    }

    frenar(){
        this.velocidad-=5;
        console.log("El coche "+this.marca+" ha frenado, ahora va a "+this.velocidad+" Km/h");
    }

    get speedUS(){
        return this.velocidad/1.6;
    }

    set speedUS(velocidad){
        this.velocidad=velocidad*1.6;
    }

}

var nissan=new Car("nissan",100);
var toyota=new Car("toyota",90);
var gundam=new Car("gundam",80);

nissan.acelerar();
toyota.frenar();
gundam.acelerar();

nissan.speedUS=40;
console.log("El coche "+nissan.marca+" va a "+nissan.speedUS+" mi/h");
toyota.speedUS=60;
console.log("El coche "+toyota.marca+" va a "+toyota.speedUS+" mi/h");
gundam.speedUS=50;
console.log("El coche "+gundam.marca+" va a "+gundam.speedUS+" mi/h");
