
class Car{

    constructor(marca, velocidadInicial){
        this.marca=marca;
        this.velocidad=velocidadInicial
    }

    acelerar(){
        this.velocidad+=10;
        console.log("El coche "+this.marca+" ha acelerado, ahora va a "+this.velocidad+" Km/h");
    }

    frenar(){
        this.velocidad-=5;
        console.log("El coche "+this.marca+" ha frenado, ahora va a "+this.velocidad+" Km/h");
    }
}

class EV extends Car{

    constructor(marca, velocidadInicial){
        super(marca, velocidadInicial)
        this.charge=100;
    }

    acelerar(){
        this.velocidad+=20;
        this.charge-=1;
        console.log("El coche "+this.marca+" ha acelerado, ahora va a "+this.velocidad+" Km/h con una bateria del "+this.charge+"%");
    }

    chargeBattery(battery){
        this.charge=battery;
    }

}

console.log("Ejercicio con clases")

var nissan=new Car("nissan",100);
var toyota=new Car("toyota",90);
var gundam=new EV("gundam",80);

nissan.acelerar();
toyota.frenar();
gundam.acelerar();
gundam.frenar();

gundam.chargeBattery(90);
gundam.acelerar();


console.log("Ejercicio con prototypos")

const CarProto =function(marca, velocidadInicial){
    this.marca=marca;
    this.velocidad=velocidadInicial
}
CarProto.prototype.acelerar=function(){
    this.velocidad+=10;
    console.log("El coche "+this.marca+" ha acelerado, ahora va a "+this.velocidad+" Km/h");
}
CarProto.prototype.frenar=function(){
    this.velocidad-=5;
    console.log("El coche "+this.marca+" ha frenado, ahora va a "+this.velocidad+" Km/h");
}

const EVProto=function(marca, velocidadInicial){
    CarProto.call(this,marca,velocidadInicial);
    this.charge=100;
}

EVProto.prototype = Object.create(CarProto.prototype);

EVProto.prototype.acelerar=function(){
    this.velocidad+=20;
    this.charge-=1;
    console.log("El coche "+this.marca+" ha acelerado, ahora va a "+this.velocidad+" Km/h con una bateria del "+this.charge+"%");
};

EVProto.prototype.chargeBattery=function(battery){
    this.charge=battery;
};

nissanP=new CarProto("nissanP",100);
toyotaP=new CarProto("toyotaP",90);
gundamP=new EVProto("gundamP",80);

nissanP.acelerar();
toyotaP.frenar();
gundamP.acelerar();
gundamP.frenar();

gundamP.chargeBattery(90);
gundamP.acelerar();
