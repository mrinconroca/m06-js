
class Gundam{
    nombre="gundamu";
}

var array=[new Gundam(),null,new Gundam(),undefined];

var isAllString=array.every(function(element){
    return typeof element == "string";
});

var isAllNumber=array.every(function(element){
    return typeof element == "number";
});

var isAllOther=array.every(function(element){
    var type=typeof element;
    return type != "string" && type != "number";
});

if(isAllString){
    window.alert("El array solo tiene Strings");
}else if(isAllNumber){
    window.alert("El array solo tiene Number");
}else if(isAllOther){
    var nulls=[];
    var undefineds=[];
    for(var i=0;i!=-1;){
        i=array.indexOf(null,i+1);
        if(i!=-1){
            nulls.push(i);
        }
    }
    for(var i=0;i!=-1;){
        i=array.indexOf(undefined,i+1);
        if(i!=-1){
            undefineds.push(i);
        }
    }
    window.alert("El array no tiene ni Strings ni Number\nLas posiciones de los nulls: "+nulls.toString()+"\nLas posiciones de los undefined: "+undefineds.toString());
}else{
    window.alert("El array tiene tipos mezclados")
}