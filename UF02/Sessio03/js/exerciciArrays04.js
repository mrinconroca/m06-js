
var array=["macarrones","con","tomactico","gandamu"];

var isAllString=array.every(function(element){
    return typeof element ;
});

var isAllNumber=array.every(function(element){
    return typeof element == "number";
});

var isAllOther=array.every(function(element){
    var type=typeof element;
    return type != "string" && type != "number";
});

if(isAllString){
    window.alert("El array solo tiene Strings");
}else if(isAllNumber){
    window.alert("El array solo tiene Number");
}else if(isAllOther){
    window.alert("El array no tiene ni Strings ni Number");
}else{
    window.alert("El array tiene tipos mezclados")
}