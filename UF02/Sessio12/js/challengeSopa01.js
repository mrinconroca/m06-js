
const Sopa = function () {
    this.sopa = [
        [, , , , , , , , , , , , , , , , , , ,],
        [, , , , , , , , , , , , , , , , , , ,],
        [, , , , , , , , , , , , , , , , , , ,],
        [, , , , , , , , , , , , , , , , , , ,],
        [, , , , , , , , , , , , , , , , , , ,],
        [, , , , , , , , , , , , , , , , , , ,],
        [, , , , , , , , , , , , , , , , , , ,],
        [, , , , , , , , , , , , , , , , , , ,],
        [, , , , , , , , , , , , , , , , , , ,],
        [, , , , , , , , , , , , , , , , , , ,],
        [, , , , , , , , , , , , , , , , , , ,],
        [, , , , , , , , , , , , , , , , , , ,],
        [, , , , , , , , , , , , , , , , , , ,],
        [, , , , , , , , , , , , , , , , , , ,],
        [, , , , , , , , , , , , , , , , , , ,],
        [, , , , , , , , , , , , , , , , , , ,],
        [, , , , , , , , , , , , , , , , , , ,],
        [, , , , , , , , , , , , , , , , , , ,],
        [, , , , , , , , , , , , , , , , , , ,],
        [, , , , , , , , , , , , , , , , , , ,],
        [, , , , , , , , , , , , , , , , , , ,]];

    this.solucio = [
        [, , , , , , , , , , , , , , , , , , ,],
        [, , , , , , , , , , , , , , , , , , ,],
        [, , , , , , , , , , , , , , , , , , ,],
        [, , , , , , , , , , , , , , , , , , ,],
        [, , , , , , , , , , , , , , , , , , ,],
        [, , , , , , , , , , , , , , , , , , ,],
        [, , , , , , , , , , , , , , , , , , ,],
        [, , , , , , , , , , , , , , , , , , ,],
        [, , , , , , , , , , , , , , , , , , ,],
        [, , , , , , , , , , , , , , , , , , ,],
        [, , , , , , , , , , , , , , , , , , ,],
        [, , , , , , , , , , , , , , , , , , ,],
        [, , , , , , , , , , , , , , , , , , ,],
        [, , , , , , , , , , , , , , , , , , ,],
        [, , , , , , , , , , , , , , , , , , ,],
        [, , , , , , , , , , , , , , , , , , ,],
        [, , , , , , , , , , , , , , , , , , ,],
        [, , , , , , , , , , , , , , , , , , ,],
        [, , , , , , , , , , , , , , , , , , ,],
        [, , , , , , , , , , , , , , , , , , ,]];
}

Sopa.prototype.afegirParaules = function () {

    this.paraules = [];
    for (var i = 0; i < 5; i++) {
        this.paraules.push(window.prompt("Escribe una palabra:").trim().replace(" ", ""));
    }
    console.log("Palabras: " + this.paraules.toString());
}

Sopa.prototype.emplenar = function () {
    var abc = "abcdefghijklmnopqrstuvwxyz".toUpperCase();
    for (let i = 0; i < 20; i++) {
        for (let j = 0; j < 20; j++) {
            if (this.sopa[i][j] == null) {
                this.sopa[i][j] = abc.charAt(Math.floor(Math.random() * abc.length));
            }
        }
    }
}

/*
    type:
        V = vertical
        H = horizontal
        D1 = Diagonal arriba abajo
        D2 = Diagonal abajo arriba
        I al final para invertir

*/
Sopa.prototype.insertarPalabra = function (palabra, type) {
    palabra = palabra.toUpperCase();
    if (type.charAt(type.length - 1) == "I") {
        palabra = (palabra + "").split("").reverse().join("");
        type = type.substring(0, type.length - 1);
    }

    var insert = false;
    var x;
    var y;

    while (!insert) {
        x = Math.floor(Math.random() * 20);
        y = Math.floor(Math.random() * 20);
        if (19 - x < palabra.length && (type != "V") || 19 - y < palabra.length && (type == "V" || type == "D1") || y - palabra.length < 0 && type == "D2") {
            continue;
        }
        var colocable = true;
        for (i = 0; i < palabra.length && colocable; i++) {
            if (this.sopa[y + i * ((type == "V") + (type == "D1") + (type == "D2") * -1)][x + i * (type != "V")] != null) {
                colocable = false;
            }
        }

        if (colocable) {
            for (i = 0; i < palabra.length; i++) {
                this.sopa[y + i * ((type == "V") + (type == "D1") + (type == "D2") * -1)][x + i * (type != "V")] = palabra.charAt(i);
                this.solucio[y + i * ((type == "V") + (type == "D1") + (type == "D2") * -1)][x + i * (type != "V")] = palabra.charAt(i);
            }
            insert = true;
        }
    }

}

Sopa.prototype.crearSopa = function () {
    console.log(this.paraules);
    this.insertarPalabra(this.paraules[0], "H");
    this.insertarPalabra(this.paraules[1], "HI");
    this.insertarPalabra(this.paraules[2], "V");
    this.insertarPalabra(this.paraules[3], "VI");
    this.insertarPalabra(this.paraules[4], "D1");
    this.insertarPalabra(this.paraules[5], "D2I");
    this.emplenar();

    console.table(this.sopa);
    console.table(this.solucio);
}

var sopa = new Sopa();
//sopa.afegirParaules();
sopa.paraules = ["gundam", "macarrones", "tomatico", "arreglado", "funciona", "ahora"];
sopa.crearSopa();




