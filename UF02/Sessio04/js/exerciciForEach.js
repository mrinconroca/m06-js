
const movements = [200, 450, -400, 3000, -650, -130, 70, 1300];
const movements2 = [430, 1000, 700, 50, 90];


var tret130=false;
var hasDesposit=false;
var allDeposit=true;

console.log("For normal:")

for(let i=0;i<movements.length;i++){
    if(movements[i]>0){
        console.log("Has hecho un ingreso de: "+movements[i]);
        hasDesposit=true;
    }else if(movements[i]<0){
        allDeposit=false;
        if(!tret130&&movements[i]==-130){
            tret130=true;
        }
        console.log("Has quitado: "+-movements[i]);
    }
}
if(tret130){
    console.log("Has tret 130€")
}else{
    console.log("No has tret 130€")
}

if(hasDesposit){
    console.log("Hi ha hagut deposits");
}else{
    console.log("No hi ha hagut deposits");
}

if(hasDesposit&&allDeposit){
    console.log("Tots el moviments son deposits")
}else{
    console.log("No tots el moviments son deposits")
}

console.log("For each:")

tret130=false;
hasDesposit=false;
allDeposit=true;
movements2.forEach(function(value){
    if(value>0){
        console.log("Has hecho un ingreso de: "+value);
        hasDesposit=true;
    }else if(value<0){
        allDeposit=false;
        if(!tret130&&value==-130){
            tret130=true;
        }
        console.log("Has quitado: "+-value);
    }
});

if(tret130){
    console.log("Has tret 130€")
}else{
    console.log("No has tret 130€")
}

if(hasDesposit){
    console.log("Hi ha hagut deposits");
}else{
    console.log("No hi ha hagut deposits");
}

if(hasDesposit&&allDeposit){
    console.log("Tots el moviments son deposits")
}else{
    console.log("No tots el moviments son deposits")
}
