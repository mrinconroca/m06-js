
function checkDogs(dogsJulia, dogsKate){
    var dogsJuliaCopia=dogsJulia.map(function(value){return value;}).slice(1,-2);
    var dogList=dogsJuliaCopia.concat(dogsKate);
    dogList.forEach(function(value,index){
        if(value>=3){
            console.log("Dog number "+(index+1)+" is an adult, and is "+value+" years old.");
        }else{
            console.log("Dog number "+(index+1)+" is a puppy 🐶.");
        }
    });
}

var dogsJulia=[3, 5, 2, 12, 7];
var dogsKate=[4, 1, 15, 8, 3];

console.log("Datos 1:")
checkDogs(dogsJulia,dogsKate);

var dogsJulia2=[9, 16, 6, 8, 3];
var dogsKate2=[10, 5, 6, 1, 4];

console.log("Datos 2:")
checkDogs(dogsJulia2,dogsKate2);
