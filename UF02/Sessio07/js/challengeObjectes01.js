const Car =function(marca, velocidadInicial){
    this.marca=marca;
    this.velocidad=velocidadInicial
}
Car.prototype.acelerar=function(){
    this.velocidad+=10;
    console.log("El coche "+this.marca+" ha acelerado, ahora va a "+this.velocidad+" Km/h");
}
Car.prototype.frenar=function(){
    this.velocidad-=5;
    console.log("El coche "+this.marca+" ha frenado, ahora va a "+this.velocidad+" Km/h");
}

var nissan=new Car("nissan",100);
var toyota=new Car("toyota",90);
var gundam=new Car("gundam",80);

nissan.acelerar();
toyota.frenar();
gundam.acelerar();
